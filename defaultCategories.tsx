import React, { useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage, TextInput, ScrollView, Button } from 'react-native';

const CATEGORIES_KEY = 'categories'

const initialCategories = [
  {
    name: "sushi",
    value: 0
  },
  {
    name: 'Restaurants',
    value: 0
  }
]

export default class App extends React.Component {
  state = {

    spendingText: ''
  }

  async componentDidMount() {
    const categoriesList = await this.getCategoriesListFromStorage()
    this.setState({ categoriesList: categoriesList ? categoriesList : initialCategories })
  }

  getCategoriesListFromStorage = async () => {
    return await AsyncStorage.getItem(CATEGORIES_KEY)
  }

  setStorage = async () => {
    //await AsyncStorage.setItem(CATEGORIES_KEY, JSON.stringify(this.state.categories))
    await AsyncStorage.setItem(CATEGORIES_KEY, JSON.stringify(this.state.categories))
    console.log(await AsyncStorage.getItem(CATEGORIES_KEY))

  }

  addSpendingToCategory = () => {

  }

  onChangeSpendingText = (text) => {
    this.setState({ spendingText: text })
  }

  render() {
    this.setStorage()
    return (
      <View style={styles.container} >
        <TextInput style={{ height: 40, width: "100%", borderColor: 'gray', borderWidth: 1 }} onChangeText={this.onChangeSpendingText} />
        <ScrollView>
          <Button title="Press me" color="#f194ff" onPress={() => this.addSpendingToCategory()}
          />

        </ScrollView>
      </View >
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 30,
    marginBottom: 30
  },
});
