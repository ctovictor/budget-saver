import React, { useEffect } from 'react';
import { StyleSheet, Text, View, AsyncStorage, TextInput, ScrollView, Button, Modal } from 'react-native';
import Snackbar from 'react-native-snackbar';

const CATEGORIES_KEY = 'categories'

const initialCategories = [
  {
    name: "sushi",
    value: 0
  },
  {
    name: 'Restaurants',
    value: 0
  }
]

export default class App extends React.Component {
  state = {
    spendingText: '',
    categoryText: '',
    categoriesList: initialCategories,
    resumoModalIsVisible: false,
    addCategoryModalIsVisible: false,
    resetPresses: 0
  }

  async componentDidMount() {
    const categoriesList = await this.getCategoriesListFromStorage()
    if (categoriesList) this.setState({ categoriesList })

  }

  getCategoriesListFromStorage = async () => {
    const temp = await AsyncStorage.getItem(CATEGORIES_KEY)
    return JSON.parse(temp)
  }

  saveCategoriesListToStorage = async (categoriesList) => {
    await AsyncStorage.setItem(CATEGORIES_KEY, JSON.stringify(categoriesList))
  }
  array_move = (arr: Array<any>, old_index: number, new_index: number) => {
    if (new_index >= arr.length) {
      var k = new_index - arr.length + 1;
      while (k--) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr
  }

  addSpendingToCategory = (categoryName) => {
    const { spendingText } = this.state
    const categoriesList = [...this.state.categoriesList]
    const pos = categoriesList.findIndex(category => category.name === categoryName)
    categoriesList[pos].value += Number(spendingText) || 0
    this.array_move(categoriesList, pos, 0)
    this.setState({ categoriesList, spendingText: '' })
    this.saveCategoriesListToStorage(categoriesList)
  }

  openResumoModal = () => {
    this.setState({ resumoModalIsVisible: true })
  }
  openAddCategoryModal = () => {
    this.setState({ addCategoryModalIsVisible: true })
  }
  closeResumoModal = () => {
    this.setState({ resumoModalIsVisible: false })
  }
  closeAddCategoryModal = () => {
    this.setState({ addCategoryModalIsVisible: false })
  }

  printCategoriesOnMemory = async () => {
    console.log(await this.getCategoriesListFromStorage())
  }

  onChangeSpendingText = (text) => {
    this.setState({ spendingText: text })
  }
  onChangeCategoryText = (text) => {
    this.setState({ categoryText: text })

  }
  handleClickReset = () => {
    const { resetPresses } = this.state
    let resetTimeout
    if (resetPresses === 0) {
      resetTimeout = setTimeout(() => {
        console.log('timeout ended')
        this.setState({ resetPresses: 0 })
      }, 3000)
    }
    if (resetPresses >= 2) {
      clearTimeout(resetTimeout)
      this.resetMemory()
    }
    else {
      this.setState({ resetPresses: resetPresses + 1 })
    }
  }
  resetMemory = () => {
    this.setState({
      spendingText: '',
      categoryText: '',
      categoriesList: initialCategories,
      resumoModalIsVisible: false,
      addCategoryModalIsVisible: false,
      resetPresses: 0
    })
    this.saveCategoriesListToStorage(initialCategories)
  }

  handleConfirmNewCategory = () => {
    const { categoryText } = this.state
    const categoriesList = [...this.state.categoriesList]
    categoriesList.push({ name: categoryText, value: 0 })
    this.setState({ categoriesList, categoryText: '' })
    this.saveCategoriesListToStorage(categoriesList)
    this.closeAddCategoryModal()
  }
  render() {
    const { resumoModalIsVisible, addCategoryModalIsVisible, spendingText, categoryText } = this.state
    return (
      <View style={styles.container} >
        <View style={styles.horizontal} >
          <View
            style={styles.paddedSides}
          >
            <Button
              title="Abrir resumo"
              color="#DB7093"
              onPress={() => this.openResumoModal()}
            />
          </View>
          <View
            style={styles.paddedSides}
          >
            <Button
              title="Adicionar Categoria"
              color="#DB7093"
              onPress={() => this.openAddCategoryModal()}
            />
          </View>
          <View
            style={styles.paddedSides}
          >
            <Button
              title="!"
              color="red"
              onPress={() => this.handleClickReset()}
            />
          </View>
        </View>
        <ScrollView>
          {this.state.categoriesList.map((category, index) => {
            return (
              <View
                key={index}
                style={styles.categoryButton}
              >
                <Button
                  title={category.name}
                  color="#f194ff"
                  onPress={() => this.addSpendingToCategory(category.name)}
                />
              </View>

            )
          })}
        </ScrollView>
        <TextInput
          style={styles.moneyInput}
          onChangeText={this.onChangeSpendingText}
          keyboardType="numeric"
          value={spendingText}
        />
        <Modal visible={resumoModalIsVisible}>
          <View style={styles.container} >
            <View style={styles.modal} >
              <ScrollView>
                {this.state.categoriesList.map((category, index) => {
                  return (
                    <Text key={`resumo-${index}`}>{`${category.name}: ${category.value}`}</Text>
                  )
                })}
              </ScrollView>
            </View>
            <Button title="fechar resumo" color="#f194ff" onPress={() => this.closeResumoModal()}
            />
          </View>
        </Modal>
        <Modal visible={addCategoryModalIsVisible}>
          <View style={styles.container} >
            <TextInput
              style={styles.moneyInput}
              onChangeText={this.onChangeCategoryText}
              value={categoryText}

            />
            <Button
              title="confirmar"
              color="#f194ff"
              onPress={() => this.handleConfirmNewCategory()}
            />
          </View>

        </Modal>
      </View >
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingTop: 40,
    paddingBottom: 10
  },
  modal: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 40,
    paddingBottom: 10
  },
  moneyInput: {
    height: 40,
    width: "100%",
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 30,
    marginBottom: 30
  },
  categoryButton: {
    marginTop: 30,
    width: '100%'
  },
  fullWidth: {
    width: '100%'
  },
  horizontal: {
    flexDirection: 'row'
  },
  paddedSides: {
    paddingLeft: 10,
    paddingRight: 10
  }
});
